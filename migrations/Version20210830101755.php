<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210830101755 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            'INSERT INTO `user` (`id`, `email`, `password`) VALUES (1, "johndoe@gmail.com", "MDhnSjVQdWdzdHRmMjFoV0ljdnFTUT09")'
        );
    }

    public function down(Schema $schema): void
    {
    }
}
