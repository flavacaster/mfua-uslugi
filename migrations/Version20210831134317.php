<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Entity\Catalog\CatalogEntry;
use App\Entity\Catalog\CatalogEntryField;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210831134317 extends AbstractMigration implements ContainerAwareInterface
{
    private EntityManagerInterface $entityManager;

    public function getDescription(): string
    {
        return '';
    }

    public function setContainer(ContainerInterface $container = null): void
    {
        /** @var EntityManagerInterface $entityManager */
        $entityManager = $container->get('doctrine.orm.entity_manager');

        $this->entityManager = $entityManager;
    }

    public function up(Schema $schema): void
    {
        $this->entityManager->transactional(
            function () {
                $catalogEntry = (new CatalogEntry())
                    ->setTitle('Справка по форме №1')
                    ->setDuration(3600);

                $this->entityManager->persist($catalogEntry);

                $field = (new CatalogEntryField())
                    ->setCatalogEntry($catalogEntry)
                    ->setName('studentPassportNumber')
                    ->setTitle('Номер студенческого билета');

                $this->entityManager->persist($field);

                $field = (new CatalogEntryField())
                    ->setCatalogEntry($catalogEntry)
                    ->setName('age')
                    ->setTitle('Возраст');

                $this->entityManager->persist($field);
            }
        );
    }

    public function down(Schema $schema): void
    {
    }
}
