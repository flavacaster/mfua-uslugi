<?php

namespace App\Config;

class UrlConfig
{
    private string $appBaseUrl;

    public function __construct(string $appBaseUrl)
    {
        $this->appBaseUrl = $appBaseUrl;
    }

    public function getAppBaseUrl(): string
    {
        return $this->appBaseUrl;
    }
}