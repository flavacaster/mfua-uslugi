<?php

namespace App\Controller;

use App\Helper\StringHelper;
use App\Library\ExternalRequestResolver\Interfaces\RequestPayloadInterface;
use App\Library\ExternalRequestResolver\Service\RequestResolver;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\PropertyNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\ConstraintViolationList;

class AbstractController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    private RequestResolver $requestResolver;

    public function __construct(RequestResolver $requestResolver)
    {
        $this->requestResolver = $requestResolver;
    }

    protected function respondSuccess($payload = null, int $statusCode = Response::HTTP_OK): Response
    {
        return $this->respond($this->prepareSuccessResponsePayload($payload, $statusCode), $statusCode);
    }

    protected function respondWithViolationList(ConstraintViolationList $list): Response
    {
        $result = [];

        foreach ($list as $item) {
            $key = StringHelper::removeFirstOccurrence($item->getPropertyPath(), ['[', ']']);

            $result[$key] = $item->getMessage();
        }

        return $this->respondFailure($result);
    }

    protected function respondFailure($payload, int $statusCode = Response::HTTP_BAD_REQUEST): Response
    {
        return $this->respond($this->prepareFailureResponsePayload($payload, $statusCode), $statusCode);
    }

    protected function respondWithCollection(array $collection, string $dtoClassName): Response
    {
        $result = [];

        foreach ($collection as $item) {
            $result[] = new $dtoClassName($item);
        }

        return $this->respondSuccess($result);
    }

    protected function resolveRequest(Request $request, string $requestClass): RequestPayloadInterface
    {
        return $this->requestResolver->resolve($request, $requestClass);
    }

    private function prepareSuccessResponsePayload($payload, int $statusCode): array
    {
        return $this->prepareResponsePayload($payload, $statusCode, 'ok');
    }

    private function prepareResponsePayload($payload, int $statusCode, string $statusText): array
    {
        return [
            'status' => $statusText,
            'code' => $statusCode,
            'data' => $payload,
        ];
    }

    #[Pure] private function prepareFailureResponsePayload($payload, int $statusCode): array
    {
        return $this->prepareResponsePayload($payload, $statusCode, 'error');
    }

    private function respond($data, int $statusCode): JsonResponse
    {
        $serializer = new Serializer([new PropertyNormalizer(), new ObjectNormalizer()], [new JsonEncoder()]);

        $payload = $serializer->serialize($data, 'json', [PropertyNormalizer::PRESERVE_EMPTY_OBJECTS => true]);

        return new JsonResponse($payload, $statusCode, [], true);
    }
}