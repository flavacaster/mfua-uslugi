<?php

namespace App\Controller\Api;

use App\Controller\AbstractController;
use App\Entity\User\User;

abstract class AbstractApiController extends AbstractController
{
    protected function getUser(): ?User
    {
        /** @var User|null $result */
        $result = parent::getUser();

        return $result;
    }
}