<?php

namespace App\Controller\Api;

use App\Repository\User\UserRepository;
use App\Request\Api\Auth\AuthRequest;
use App\Security\EncodeService;
use App\Security\TokenSecurityService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class AuthController extends AbstractApiController
{
    #[Route(path: "/api/v1/auth", methods: ["POST", "GET"])]
    public function login(
        Request $httpRequest,
        UserRepository $userRepository,
        EncodeService $encodeService,
        TokenSecurityService $tokenSecurityService
    ): Response {
        /** @var AuthRequest $request */
        $request = $this->resolveRequest($httpRequest, AuthRequest::class);

        $user = $userRepository->findOneBy(['email' => $request->getEmail()]);

        if (!$user) {
            return $this->respondFailure('Invalid credentials');
        }

        $hashedPassword = $encodeService->encode($request->getPassword());

        if ($hashedPassword != $user->getPassword()) {
            return $this->respondFailure('Invalid credentials');
        }

        return $this->respondSuccess(
            [
                'token' => $tokenSecurityService->createToken($user)
            ]
        );
    }
}