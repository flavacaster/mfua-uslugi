<?php

namespace App\Controller\Api;

use App\Dto\Api\Catalog\CatalogEntryDto;
use App\Dto\Api\Catalog\CatalogRequestDto;
use App\Entity\Catalog\CatalogEntryField;
use App\Entity\Catalog\Request as CatalogRequest;
use App\Repository\Catalog\CatalogEntryRepository;
use App\Repository\Catalog\RequestRepository;
use App\Request\Api\Catalog\CatalogSubmitRequest;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CatalogController extends AbstractApiController
{
    #[Route(path: "/api/v1/catalog", methods: "GET")]
    public function getAll(
        CatalogEntryRepository $repository
    ): Response {
        return $this->respondWithCollection($repository->findAll(), CatalogEntryDto::class);
    }

    #[Route(path: "/api/v1/catalog/submit", methods: "POST")]
    public function submit(
        Request $httpRequest,
        CatalogEntryRepository $catalogEntryRepository,
        EntityManagerInterface $entityManager
    ): Response {
        /** @var CatalogSubmitRequest $request */
        $request = $this->resolveRequest($httpRequest, CatalogSubmitRequest::class);

        $catalogEntry = $catalogEntryRepository->findOneBy(['id' => $request->getId()]);

        $fieldData = $request->getFields();
        $validatedFields = [];

        /** @var CatalogEntryField $field */
        foreach ($catalogEntry->getFields()->toArray() as $field) {
            if (!array_key_exists($field->getName(), $fieldData)) {
                return $this->respondFailure(
                    [
                        sprintf('fields[%s]', $field->getName()) => 'Field is missing'
                    ]
                );
            }

            $fieldValue = $fieldData[$field->getName()];

            if ($fieldValue == '' || $fieldValue == null) {
                return $this->respondFailure(
                    [
                        $field->getName() => 'Field cannot be empty'
                    ]
                );
            }

            $validatedFields[$field->getTitle()] = $fieldValue;
        }

        $catalogRequest = (new CatalogRequest())
            ->setStatus(CatalogRequest::STATUS_NEW)
            ->setCatalogEntry($catalogEntry)
            ->setTitle($catalogEntry->getTitle())
            ->setCreatedAt(new DateTime('now'))
            ->setUser($this->getUser())
            ->setFields($validatedFields);

        $entityManager->persist($catalogRequest);

        $entityManager->flush();

        return $this->respondSuccess(['id' => $catalogRequest->getId()]);
    }

    #[Route(path: "/api/v1/catalog/orders", methods: "GET")]
    public function orders(
        RequestRepository $requestRepository
    ): Response {
        return $this->respondWithCollection(
            $requestRepository->findBy(['user' => $this->getUser()]),
            CatalogRequestDto::class
        );
    }
}