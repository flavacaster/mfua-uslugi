<?php

namespace App\Controller\Manager;

use App\Config\UrlConfig;
use App\Dto\Manager\CatalogRequestDto;
use App\Entity\Catalog\Request as CatalogRequest;
use App\Exception\Controller\Manager\InvalidCatalogRequestIdException;
use App\Repository\Catalog\RequestRepository;
use App\Request\Manager\Catalog\UploadDocumentRequest;
use Doctrine\ORM\EntityManagerInterface;
use League\Flysystem\Filesystem;
use Symfony\Component\HttpFoundation\Request as HttpRequest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Uid\Uuid;
use Symfony\Component\Validator\Constraints\Image;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class CatalogController extends AbstractManagerController
{
    #[Route(path: "/manager/v1/catalog", methods: "GET")]
    public function getAll(
        RequestRepository $requestRepository
    ): Response {
        return $this->respondWithCollection($requestRepository->findAll(), CatalogRequestDto::class);
    }

    #[Route(path: "/manager/v1/catalog/approve")]
    public function approve(
        HttpRequest $httpRequest,
        RequestRepository $requestRepository,
        EntityManagerInterface $entityManager
    ): Response {
        try {
            $catalogRequest = $this->getValidateCatalogRequest($httpRequest, $requestRepository);
        } catch (InvalidCatalogRequestIdException $e) {
            return $this->respondFailure(['id' => $e->getMessage()]);
        }

        $catalogRequest->setStatus(CatalogRequest::STATUS_DONE);

        $entityManager->persist($catalogRequest);
        $entityManager->flush();

        return $this->respondSuccess();
    }

    #[Route(path: "/manager/v1/catalog/decline")]
    public function decline(
        HttpRequest $httpRequest,
        RequestRepository $requestRepository,
        EntityManagerInterface $entityManager
    ): Response {
        try {
            $catalogRequest = $this->getValidateCatalogRequest($httpRequest, $requestRepository);
        } catch (InvalidCatalogRequestIdException $e) {
            return $this->respondFailure(['id' => $e->getMessage()]);
        }

        $catalogRequest->setStatus(CatalogRequest::STATUS_DECLINED);

        $entityManager->persist($catalogRequest);
        $entityManager->flush();

        return $this->respondSuccess();
    }

    #[Route(path: "/manager/v1/catalog/upload")]
    public function uploadDocument(
        HttpRequest $httpRequest,
        ValidatorInterface $validator,
        RequestRepository $requestRepository,
        EntityManagerInterface $entityManager,
        Filesystem $filesystem,
        UrlConfig $urlConfig
    ): Response {
        try {
            $catalogRequest = $this->getValidateCatalogRequest($httpRequest, $requestRepository);
        } catch (InvalidCatalogRequestIdException $e) {
            return $this->respondFailure(['id' => $e->getMessage()]);
        }

        /** @var UploadDocumentRequest $request */
        $request = $this->resolveRequest($httpRequest, UploadDocumentRequest::class);

        $documentContent = explode('base64,', $request->getDocument())[1] ?? '';
        $documentContent = base64_decode($documentContent);
        $extension = explode('/', (explode('base64,', $request->getDocument())[0] ?? ''))[1] ?? '';
        $extension = trim($extension, ';');

        $documentName = sprintf('%s.%s', Uuid::v4(), $extension);

        $filesystem->write(sprintf('tmp/%s', $documentName), $documentContent);

        $violations = $validator->validate(
            __DIR__ . '/../../../public/upload/tmp/' . $documentName,
            [new NotBlank(), new Image()]
        );

        if ($violations->count()) {
            return $this->respondWithViolationList($violations);
        }

        $filesystem->write(
            sprintf('document/%s', $documentName),
            $documentContent
        );


        $catalogRequest->setDocumentUrl(
            strtr(
                '{baseUrl}/static/{documentName}',
                [
                    '{baseUrl}' => $urlConfig->getAppBaseUrl(),
                    '{documentName}' => $documentName
                ]
            )
        );

        $entityManager->persist($catalogRequest);
        $entityManager->flush();

        return $this->respondSuccess();
    }

    private function getValidateCatalogRequest(
        HttpRequest $httpRequest,
        RequestRepository $requestRepository
    ): CatalogRequest {
        $id = (int)$httpRequest->query->get('id');

        if (!$id) {
            throw new InvalidCatalogRequestIdException('ID is required');
        }

        $catalogRequest = $requestRepository->find(['id' => $id]);

        if (!$catalogRequest) {
            throw new InvalidCatalogRequestIdException('Invalid ID');
        }

        if ($catalogRequest->getStatus() != CatalogRequest::STATUS_NEW) {
            throw new InvalidCatalogRequestIdException('Order was already processed');
        }

        return $catalogRequest;
    }
}