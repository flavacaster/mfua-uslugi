<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StaticController extends AbstractController
{
    /**
     * @Route(path="/static/{file}", requirements={"file":".*"}, methods={"GET"})
     */
    public function serve(string $file): Response
    {
        $fullFileName = sprintf('/app/public/upload/document/%s', $file);

        if (!file_exists($fullFileName)) {
            return new Response(null, Response::HTTP_NOT_FOUND);
        }

        return new BinaryFileResponse($fullFileName);
    }
}