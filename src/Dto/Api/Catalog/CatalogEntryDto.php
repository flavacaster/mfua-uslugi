<?php

namespace App\Dto\Api\Catalog;

use App\Entity\Catalog\CatalogEntry;

class CatalogEntryDto
{
    private int $id;
    private string $title;
    private int $duration;
    /** @var CatalogEntryFieldDto[] */
    private array $fields;

    public function __construct(CatalogEntry $catalogEntry)
    {
        $this->id = (int)$catalogEntry->getId();
        $this->title = (string)$catalogEntry->getTitle();
        $this->duration = (int)$catalogEntry->getDuration();

        foreach ($catalogEntry->getFields()->toArray() as $field) {
            $this->fields[] = new CatalogEntryFieldDto($field);
        }
    }


}