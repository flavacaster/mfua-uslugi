<?php

namespace App\Dto\Api\Catalog;

use App\Entity\Catalog\CatalogEntryField;
use JetBrains\PhpStorm\Pure;

class CatalogEntryFieldDto
{
    private string $name;
    private string $title;

    #[Pure] public function __construct(CatalogEntryField $catalogEntryField)
    {
        $this->name = (string)$catalogEntryField->getName();
        $this->title = (string)$catalogEntryField->getTitle();
    }
}