<?php

namespace App\Dto\Manager;

use App\Entity\Catalog\Request;

class CatalogRequestDto
{
    private int $id;
    private string $title;
    private int $status;
    private int $dateOrdered;
    private array $fields;
    private ?string $documentUrl;

    public function __construct(Request $catalogRequest)
    {
        $this->id = (int)$catalogRequest->getId();
        $this->title = (string)$catalogRequest->getTitle();
        $this->status = (int)$catalogRequest->getStatus();
        $this->dateOrdered = $catalogRequest->getCreatedAt()->getTimestamp();
        $this->fields = (array)$catalogRequest->getFields();
        $this->documentUrl = $catalogRequest->getDocumentUrl();
    }

}