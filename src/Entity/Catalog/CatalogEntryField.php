<?php

namespace App\Entity\Catalog;

use App\Repository\Catalog\CatalogEntryFieldRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CatalogEntryFieldRepository::class)
 */
class CatalogEntryField
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity=CatalogEntry::class, inversedBy="fields")
     * @ORM\JoinColumn(nullable=false)
     */
    private $catalogEntry;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCatalogEntry(): ?CatalogEntry
    {
        return $this->catalogEntry;
    }

    public function setCatalogEntry(?CatalogEntry $catalogEntry): self
    {
        $this->catalogEntry = $catalogEntry;

        return $this;
    }
}
