<?php

namespace App\Exception\Controller\Manager;

use Exception;

class InvalidCatalogRequestIdException extends Exception
{
}