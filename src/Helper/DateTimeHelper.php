<?php

namespace App\Helper;

class DateTimeHelper
{
    public const DEFAULT_FORMAT = 'Y-m-d H:i:s';
}