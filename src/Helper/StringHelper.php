<?php

namespace App\Helper;

class StringHelper
{
    public static function removeFirstOccurrence(string $string, $search): string
    {
        $search = (array)$search;

        $result = $string;
        foreach ($search as $element) {
            $index = strpos($result, $element);
            if ($index === false) {
                continue;
            }

            $result = substr($result, 0, $index) . substr($result, $index + strlen($element));
        }

        return $result;
    }
}