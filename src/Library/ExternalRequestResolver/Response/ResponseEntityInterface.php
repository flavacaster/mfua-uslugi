<?php

declare(strict_types=1);

namespace App\Library\ExternalRequestResolver\Response;

interface ResponseEntityInterface
{
    public static function getEntityName(): string;
}
