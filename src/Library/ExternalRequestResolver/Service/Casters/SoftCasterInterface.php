<?php

declare(strict_types=1);

namespace App\Library\ExternalRequestResolver\Service\Casters;

interface SoftCasterInterface
{
    public function cast($value);
}
