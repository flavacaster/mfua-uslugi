<?php

declare(strict_types=1);

namespace App\Library\ExternalRequestResolver\Service\Casters;

final class SoftStringCaster implements SoftCasterInterface
{
    public function cast($value)
    {
        if (! is_scalar($value)) {
            return $value;
        }

        return (string) $value;
    }
}
