<?php

namespace App\Listener\Exception;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

class AppExceptionListener
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();

        $this->handleUnauthorizedException($event, $exception);

        if ($event->isPropagationStopped()) {
            return;
        }

        $this->handleJsonPrettyPrinting($event, $exception);
    }

    private function handleUnauthorizedException(ExceptionEvent $event, Throwable $exception): void
    {
        if ($exception instanceof HttpException && $exception->getStatusCode() == Response::HTTP_UNAUTHORIZED) {
            $event->setResponse(
                new JsonResponse(
                    [
                        'status' => 'error',
                        'code' => Response::HTTP_UNAUTHORIZED,
                        'data' => 'Not authorized'
                    ],
                    Response::HTTP_UNAUTHORIZED
                )
            );
        }
    }

    private function handleJsonPrettyPrinting(ExceptionEvent $event, Throwable $exception): void
    {
        /** @var array $contentType */
        $contentType = $event->getRequest()->headers->get('content-type');

        if ($contentType == 'application/json') {
            $response = new JsonResponse(
                [
                    'title' => 'Error happened',
                    'message' => $exception->getMessage(),
                    'trace' => $exception->getTrace(),
                    'dupMessage' => $exception->getMessage(),
                ],
                JsonResponse::HTTP_INTERNAL_SERVER_ERROR
            );

            $event->setResponse($response);
        }
    }

}