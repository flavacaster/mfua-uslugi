<?php

namespace App\Repository\Catalog;

use App\Entity\Catalog\CatalogEntryField;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CatalogEntryField|null find($id, $lockMode = null, $lockVersion = null)
 * @method CatalogEntryField|null findOneBy(array $criteria, array $orderBy = null)
 * @method CatalogEntryField[]    findAll()
 * @method CatalogEntryField[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CatalogEntryFieldRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CatalogEntryField::class);
    }
}
