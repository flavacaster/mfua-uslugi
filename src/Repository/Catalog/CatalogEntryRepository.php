<?php

namespace App\Repository\Catalog;

use App\Entity\Catalog\CatalogEntry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CatalogEntry|null find($id, $lockMode = null, $lockVersion = null)
 * @method CatalogEntry|null findOneBy(array $criteria, array $orderBy = null)
 * @method CatalogEntry[]    findAll()
 * @method CatalogEntry[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CatalogEntryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CatalogEntry::class);
    }
}
