<?php

namespace App\Request\Api\Auth;

use App\Library\ExternalRequestResolver\Interfaces\RequestPayloadInterface;
use Symfony\Component\Validator\Constraints;

class AuthRequest implements RequestPayloadInterface
{
    /**
     * @Constraints\Type(type="string")
     * @Constraints\Email()
     */
    private string $email;

    /**
     * @Constraints\Type(type="string")
     */
    private string $password;

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getPassword(): string
    {
        return $this->password;
    }
}