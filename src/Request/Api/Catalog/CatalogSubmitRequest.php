<?php

namespace App\Request\Api\Catalog;

use App\Library\ExternalRequestResolver\Interfaces\RequestPayloadInterface;
use App\Validation\Common\Exists;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints;
use App\Entity\Catalog\CatalogEntry;

class CatalogSubmitRequest implements RequestPayloadInterface
{
    /**
     * @Constraints\Type("integer")
     * @Exists(model=CatalogEntry::class, column="id")
     */
    private int $id;

    /**
     * @Serializer\Type("array")
     * @Constraints\NotBlank()
     */
    private array $fields;

    public function getId(): int
    {
        return $this->id;
    }

    public function getFields(): array
    {
        return $this->fields;
    }
}