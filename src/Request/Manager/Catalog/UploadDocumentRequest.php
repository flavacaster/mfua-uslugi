<?php

namespace App\Request\Manager\Catalog;

use App\Library\ExternalRequestResolver\Interfaces\RequestPayloadInterface;
use Symfony\Component\Validator\Constraints;

class UploadDocumentRequest implements RequestPayloadInterface
{

    /**
     * @Constraints\Type("string")
     */
    private string $document;

    public function getDocument(): string
    {
        return $this->document;
    }
}