<?php

namespace App\Security;

class EncodeService
{
    private const ENCRYPT_METHOD = "AES-256-CBC";
    private const TOKEN_ENCODE_SALT = 'hffd123Sdgh8yo-there';
    private const INITIALISATION_VECTOR = 'U2FsdGVkX18/Y0NXLsO+';

    public function encode(string $data): string
    {
        $output = openssl_encrypt(
            $data,
            self::ENCRYPT_METHOD,
            $this->createHash(),
            0,
            $this->createIv()
        );

        return base64_encode($output);
    }

    public function decode(string $data): ?string
    {
        $result = openssl_decrypt(
            base64_decode($data),
            self::ENCRYPT_METHOD,
            $this->createHash(),
            0,
            $this->createIv()
        );

        return $result ?: null;
    }

    private function createHash(): string
    {
        return hash('sha256', self::TOKEN_ENCODE_SALT);
    }

    private function createIv(): string
    {
        return substr(hash('sha256', self::INITIALISATION_VECTOR), 0, 16);
    }
}