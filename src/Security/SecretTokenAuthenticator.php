<?php

namespace App\Security;

use App\Repository\User\UserRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class SecretTokenAuthenticator extends AbstractAuthenticator
{
    private UserRepository $userRepository;
    private TokenSecurityService $tokenSecurityService;

    public function __construct(UserRepository $userRepository, TokenSecurityService $tokenSecurityService)
    {
        $this->userRepository = $userRepository;
        $this->tokenSecurityService = $tokenSecurityService;
    }

    public function supports(Request $request): ?bool
    {
        return $this->hasBearerToken($request);
    }

    public function authenticate(Request $request): PassportInterface
    {
        $tokenData = $this->parseTokenDataFromRequest($request);

        if (!$tokenData) {
            throw new BadCredentialsException;
        }

        $user = $this->userRepository->findOneBy(
            [
                'id' => $tokenData['id'],
                'email' => $tokenData['email']
            ]
        );

        if (!$user) {
            throw new UserNotFoundException();
        }

        return new SelfValidatingPassport(
            new UserBadge(
                $user->getId(),
                function () use ($user) {
                    return $user;
                }
            )
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return new JsonResponse(
            [
                'status' => 'error',
                'code' => Response::HTTP_UNAUTHORIZED,
                'data' => 'Not authorized'
            ],
            Response::HTTP_UNAUTHORIZED
        );
    }

    private function hasBearerToken(Request $request): bool
    {
        if (!$request->headers->has('Authorization')) {
            return false;
        }

        $value = $request->headers->get('Authorization');

        return strtolower((explode(' ', $value)[0] ?? '')) == 'bearer';
    }

    private function parseTokenDataFromRequest(Request $request): ?array
    {
        $authHeaderValue = $request->headers->get('Authorization');

        if (!$authHeaderValue) {
            return null;
        }

        $token = $this->parseToken($authHeaderValue);

        if (!$token) {
            return null;
        }

        return $this->tokenSecurityService->decodeToken($token);
    }

    private function parseToken(string $data): ?string
    {
        return explode(' ', $data)[1] ?? null;
    }

}
