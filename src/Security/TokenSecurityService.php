<?php

namespace App\Security;

use App\Entity\User\User;

class TokenSecurityService
{
    public const DEFAULT_USER_PASSWORD_SALT = 'Y0NXLsO+/U2FsdGVkX18';

    private EncodeService $encodeService;

    public function __construct(EncodeService $encodeService)
    {
        $this->encodeService = $encodeService;
    }

    public function createToken(User $user): string
    {
        $tokenData = json_encode(['id' => $user->getId(), 'email' => $user->getEmail()]);

        return $this->encodeService->encode($tokenData);
    }

    public function decodeToken(string $token): ?array
    {
        $result = $this->encodeService->decode($token);

        if (!$result) {
            return null;
        }

        return json_decode($result, true);
    }
}