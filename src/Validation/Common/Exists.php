<?php

namespace App\Validation\Common;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class Exists extends Constraint
{
    public string $model;
    public string $column;

    public string $errorMessage = 'Entity does not exist';
}