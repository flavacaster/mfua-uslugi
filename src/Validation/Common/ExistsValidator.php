<?php

namespace App\Validation\Common;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ExistsValidator extends ConstraintValidator
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param mixed $value
     * @param Exists $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        $repository = $this->entityManager->getRepository($constraint->model);

        if (!$repository->findOneBy([$constraint->column => $value])) {
            $this->context
                ->buildViolation($constraint->errorMessage)
                ->addViolation();
        }
    }

}